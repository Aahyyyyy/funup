package com.letsfun.funup;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FunupApplication {

    public static void main(String[] args) {
        SpringApplication.run(FunupApplication.class, args);
    }

}
